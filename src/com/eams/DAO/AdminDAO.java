package com.eams.DAO;

import java.util.List;
import com.eams.bean.Admin;
/**
 * This is the AdminDAO interface where various Admin Abstract methods are created.
 * @author Batch - G
 *
 */
public interface AdminDAO {
	/**
	 * this method is used to insert a new admin into database.
	 * @param adminDetails
	 * @return
	 */
	boolean createAdminDAO( Admin adminDetails );
	/**
	 *  this method is used to search an admin from the database.
	 * @param username
	 * @return
	 */
	Admin searchByUsername(String username);
	/**
	 * this method is used to delete an admin from the database.
	 * @param username
	 * @return
	 */
	boolean deleteAdminDAO( String username );
}
