package com.eams.DAO;

import java.sql.Date;
import java.util.List;
import com.eams.bean.Employee;
import com.eams.bean.Leave;
/**
 * This is the EmployeeDAO interface where various Employee Abstract methods are created.
 * @author Batch - G
 *
 */
public interface EmployeeDAO {
	/**
	 * This method is used to register a new employee.
	 * @param employee
	 * @return
	 */
	boolean insertEmployeeDAO( Employee employee );
	/**
	 * This method is used to search an employee by using emp_id.
	 * @param id
	 * @return
	 */
	List<Employee> findByIdDAO(int id);
	/**
	 * This method is used to Display all employee details to an admin.
	 * @return
	 */
	List<Employee> fetchAllEmployeeDAO();
	/**
	 * This method is used to delete an employee by using emp_id.
	 * @param id
	 * @return
	 */
	boolean deleteEmployeeDAO( int id );
	/**
	 * This method is used to update an employee details by using emp_id.
	 * @param emp_id
	 * @param phone_number
	 * @return
	 */
	boolean updateEmployeeDAO (int emp_id, String phone_number);
	/**
	 * This method is used to validate an employee login credentials.
	 * @param credentials1
	 * @return
	 */
	boolean employeeLogin(Employee credentials1);
	/**
	 * This method calls all the employee leave requests.
	 * @return
	 */
	List<Leave> fetchallLeaves();
	
}
