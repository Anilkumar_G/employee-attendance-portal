package com.eams.bean;
/**
 * This Admin class is used to generate setters and getter methods for admin credentials.
 * @author Batch - G
 *
 */
public class Admin {

    private int Admin_id;
    private static String Admin_username;
    private static String Admin_password;
    private String Security_ques;
    
	public int getAdmin_id() {
		return Admin_id;
	}
	public void setAdmin_id(int admin_id) {
		Admin_id = admin_id;
	}
	public String getAdmin_username() {
		return Admin_username;
	}
	public static void setAdmin_username(String admin_username) {
		Admin_username = admin_username;
	}
	public static String getAdmin_password() {
		return Admin_password;
	}
	public static void setAdmin_password(String admin_password) {
		Admin_password = admin_password;
	}
	public String getSecurity_ques() {
		return Security_ques;
	}
	public void setSecurity_ques(String security_ques) {
		Security_ques = security_ques;
	}
}
