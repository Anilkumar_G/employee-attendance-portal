package com.eams.validations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * This class is used to validate the all user input data.
 * @author IMVIZAG
 *
 */
public class Validation {
/**
 * this method calls the regex pattern for the email input validations.
 */
static final String EMAIL_REGEX = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
	
	
	
	
	/**
	 * This method is used to check the email pattern fro validation.
	 * @param emailId
	 * @return
	 */
	public  static boolean checkEmailId(String emailId)
	{
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
              "[a-zA-Z0-9_+&*-]+)*@" + 
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" + 
                "A-Z]{2,7}$"; 
                  
    Pattern pat = Pattern.compile(emailRegex); 
    return pat.matcher(emailId).matches(); 
	}
	
	 /**
   	 * this method checks the name with given regex pattern
   	 * @param NAME_REGEX
   	 * @param name
   	 * @return boolean
   	 */
	  /**
	   * Validate username with regular expression
	   * @param username username for validation
	   * @return true valid username, false invalid username
	   */
   
	  public static boolean checkName(String name) {
		  Pattern pattern;
		  Matcher matcher;
	 
		  final String USERNAME_PATTERN = "^[a-z0-9_-]{6,15}$";
		  pattern = Pattern.compile(USERNAME_PATTERN);
		  matcher = pattern.matcher(name);
		  return matcher.matches();
		  
		  
		   }
	  public  static boolean passwordCheck(String password) {
			 Pattern pattern ;
			 Matcher matcher;
			 final String PASSWORD_REGEX1 = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,10})";
			 pattern = Pattern.compile(PASSWORD_REGEX1);
			 matcher = pattern.matcher(password);
			 return  matcher .matches();   
		}
	  /**
		 *this method checks the mobile number with given regex pattern 
		 * @param PHONE_REGEX
		 * @param phNumber
		 * @return boolean
		 */
		public static boolean mobileCheck(String phoneNo) {
			final String PHONE_REGEX1 ="^\\d{10}$";
			Pattern pattern;
			 Matcher matcher;	
			 pattern = Pattern.compile(PHONE_REGEX1);
			 matcher = pattern.matcher(phoneNo);
			 return  matcher.matches();   
		}
	}