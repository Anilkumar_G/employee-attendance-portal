package com.eams.services;

import java.sql.Date;
import java.util.List;

import com.eams.bean.Employee;
import com.eams.bean.Leave;
/**
 * This is EmployeeService interface where all business logics are inserted. 
 * @author Batch - G
 *
 */
public interface EmployeeService {
	/**
	 * This method is used to register a new employee.
	 * @param employee
	 * @return
	 */
	boolean insertEmployee( Employee employee );
	/**
	 * This method is used to search an employee by using emp_id.
	 * @param id
	 * @return
	 */
	List<Employee> findById(int id);
	/**
	 * This method is used to Display all employee details to an admin.
	 * @return
	 */
	List<Employee> fetchAllEmployee();
	/**
	 * This method is used to delete an employee by using emp_id.
	 * @param id
	 * @return
	 */
	boolean deleteEmployee( int id );
	/**
	 * This method is used to update an employee details by using emp_id.
	 * @param emp_id
	 * @param phone_number
	 * @return
	 */
	boolean updateEmployee (int emp_id, String phone_number);
	/**
	 * This method is used to validate an employee login credentials.
	 * @param credentials1
	 * @return
	 */
	boolean employeeLogin(Employee credentials1);
	/**
	 * This method calls the all employee leave data from the database.
	 * @return
	 */
	List<Leave> fetchAllLeave();
}
